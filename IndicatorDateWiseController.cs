﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EntityModel.Reports;
using Model.Reports;
namespace AllianceWEBAPI.Controllers
{
    public class IndicatorDateWiseController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public HttpResponseMessage GetIndicatorDateWise(string id, string level, DateTime startDate,DateTime endDate,string orwid=null)
        {
            try
            {                               
                BUIndicator buIndicator = new BUIndicator();
                var indicator = buIndicator.getIndicatorDateWise(id,level,startDate,endDate, orwid );
                //var indicator = GetStub();
                if (indicator == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Indicator Report Data does not exist");
                }
                return Request.CreateResponse(HttpStatusCode.OK, indicator);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        
        }
        //private List<Indicator> GetStub()
        //{
        //    List<Indicator> indicator = new List<Indicator>
        //    {
        //       new Indicator {  ORW ="200300", No_of_LFU_contacted = 8, No_of_referrals_done_NonHealth = 10, No_of_Registered_Clients_Contacted = 45, Number_of_followup_cases = 34, PREV_No_of_LFU_contacted = 0, PREV_No_of_referrals_done_NonHealth = 0, PREV_No_of_Registered_Clients_Contacted = 0, PREV_Number_of_followup_cases = 0 },
        //       new Indicator {   ORW ="200301", No_of_LFU_contacted = 9, No_of_referrals_done_NonHealth = 11, No_of_Registered_Clients_Contacted = 20, Number_of_followup_cases = 10, PREV_No_of_LFU_contacted = 0, PREV_No_of_referrals_done_NonHealth = 0, PREV_No_of_Registered_Clients_Contacted = 0, PREV_Number_of_followup_cases = 0 },
        //       new Indicator { ORW ="200303", No_of_LFU_contacted = 9, No_of_referrals_done_NonHealth = 11, No_of_Registered_Clients_Contacted = 20, Number_of_followup_cases = 10, PREV_No_of_LFU_contacted = 0, PREV_No_of_referrals_done_NonHealth = 0, PREV_No_of_Registered_Clients_Contacted = 0, PREV_Number_of_followup_cases = 0 }

        //   };
        //    return indicator;
        //}
    }
}
